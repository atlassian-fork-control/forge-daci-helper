import api from "@forge/api";
import ForgeUI, {
  Fragment,
  Image,
  render,
  Text,
  useAction,
  useProductContext
} from "@forge/ui";
import pageUtil from './PageUtil';
import { ValidationResultType, ValidationResult } from "./types";
import validationUtil from './ValidationUtil';
import DaciChecker from "./DaciChecker";

const App = () => {
  const showValidatedChecks = false;
  const context = useProductContext();
  const [adf] = useAction(value => value, () => pageUtil.fetchPageADF(api, context.contentId));
  // console.log('adf:\n' + JSON.stringify(adf, null, 2));
  const daciValidation = new DaciChecker(adf)
    .setDateOfCheck(new Date())
    .check();

  const renderSummarySvg = (message) => {
    let errorCount = validationUtil.countErrors(daciValidation.results);
    let problemCount = validationUtil.countProblems(daciValidation.results);
    let borderColor = 'FIREBRICK';
    let fillColor = 'WHITE';
    let textColor = "DARKSLATEGRAY";
    if (errorCount) {
      borderColor = 'FIREBRICK';
      fillColor = 'SEASHELL';
    } else if (problemCount) {
      borderColor = 'ORANGE';
      fillColor = 'PAPAYAWHIP';
    } else {
      borderColor = 'GREEN';
      fillColor = 'HONEYDEW';
    }
    const fontFamily="Arial, Helvetica, sans-serif";
    const width = 660;
    const height = 50;
    const borderStrokeWidth = 1;
    const borderMargin = 2;
    const borderWidth = width - 2 * borderMargin;
    const borderHeight = height - 2 * borderMargin;
    const textMarginLeft = borderMargin + 10;
    const middleY = height/2;

    let countdownGraphics = '';
    const millisecondsUntilDue = daciValidation.millisecondsUntilDue;
    if (millisecondsUntilDue !== undefined) {
      let countdownTextColor = 'GREEN';
      const daysUntilDue = millisecondsUntilDue / (1000 * 60 * 60 * 24);
      let message = `Due: ${Math.round(daysUntilDue)}d`;
      if (daysUntilDue < 1) {
        message = daciValidation.overdue ? 'OVERDUE' : '';
        countdownTextColor = 'FIREBRICK';
      } else if (daysUntilDue < 3) {
        countdownTextColor = 'FIREBRICK';
      } else if (daysUntilDue < 10) {
        countdownTextColor = 'DARKORANGE';
      }
      countdownGraphics = `<text x="${width - textMarginLeft}" y="${middleY}" text-anchor="end" alignment-baseline="central" font-family="${fontFamily}" stroke="${countdownTextColor}">${message}</text>`;
    }
    const svg = `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" xml:space="preserve" width="${width}px" height="${height}px">
<g>
    <rect x="${borderMargin}" y="${borderMargin}" width="${borderWidth}" height="${borderHeight}" rx="6" stroke-width="${borderStrokeWidth}" fill="${fillColor}" stroke="${borderColor}" />
    <text x="${textMarginLeft}" y="${middleY}" text-anchor="start" alignment-baseline="central" font-family="${fontFamily}" stroke="${textColor}">${message}</text>
    ${countdownGraphics}
</g>
</svg>`;
    return (
      <Image
        src={`data:image/svg+xml;utf8,${svg}`}
        alt='Summary banner'
      />
    );
  };

  const renderValidationResults = () => {
    const renderedResults = daciValidation.results.map((result: ValidationResult) => {
      if (!showValidatedChecks && result.type === ValidationResultType.Pass) {
        return null;
      }
      const prefix = result.type === ValidationResultType.Pass ? '`/`' : '`X`';
      const message = `${prefix} ${result.message}`;
      return(<Text content={message}/>);
    }).filter((item) => {
      return item !== null;
    });
    const summaryMessage = renderedResults.length ?
      `Oh my, it looks like there are problems with this DACI!` :
      `Congratulations, this DACI passes all the validation tests. :)`;
    return (
      <Fragment>
        {renderSummarySvg(summaryMessage)}
        {renderedResults}
      </Fragment>
    );
  };

  return (
    <Fragment>
      <Text content="**DACI Helper**" />
      {renderValidationResults()}
      <Text content="**Visit [go/daci-helper](go/daci-helper) for more info about DACI Helper.**" />
    </Fragment>
  );
};

export const run = render(<App />);
