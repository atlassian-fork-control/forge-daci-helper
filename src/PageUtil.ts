
class PageUtil {

  fetchPageADF = async (api, contentId) => {
    // https://developer.atlassian.com/cloud/confluence/rest/#api-api-content-id-get
    // status=any is necessary to support pages in draft
    const contentPath = `/wiki/rest/api/content/${contentId}?status=any&expand=body.atlas_doc_format`;
    console.log('Sending query to: ' + contentPath);
    const res = await api
      .asUser()
      .requestConfluence(contentPath);
    const responseData = await res.json();

    if (responseData.code) {
      console.log('Response data:\n' + JSON.stringify(responseData, null, 2));
      return undefined;
    } else {
      const adfJson = responseData.body.atlas_doc_format.value;
      const adf = JSON.parse(adfJson);
      // console.log('ADF:\n' + JSON.stringify(adf, null, 2));
      return adf;
    }
  };

}

export default new PageUtil()
