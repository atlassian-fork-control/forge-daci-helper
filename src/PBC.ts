class PBC {

  assertTruthy = (object: any, subject: string): void => {
    if (!object) {
      this._raiseError(`Expected ${subject} to be truthy`);
    }
  };

  assertEqual = (object: any, expectedValue: any, subject: string): void => {
    this.assertTruthy(object, subject);
    if (!(object === expectedValue)) {
      this._raiseError(`Expected ${subject} to be equal to ${expectedValue}`);
    }
  };

  _raiseError = (message: string) => {
    const error = new Error(message);
    console.log(error.stack);
    throw message;
  }

}

export default new PBC()
